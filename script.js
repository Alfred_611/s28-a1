
// STEP 3 - Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API (https://jsonplaceholder.typicode.com/todos/)
fetch('https://jsonplaceholder.typicode.com/todos/').then(response => console.log(response.status));
fetch('https://jsonplaceholder.typicode.com/todos/').then((response) => response.json()).then((json) => console.log(json));


// STEP 4 - Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console
/*
async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/todos/');
	let json = await result.json()
	var map = [];
	map = json.map(item => item.title);
	console.log(map);
}
fetchData();
*/

// STEP 5 - Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API
fetch('https://jsonplaceholder.typicode.com/todos/1').then((response) => response.json()).then((json) => console.log(json));


// STEP 6 - Using the data retrieved, print a message in the console that will provide the title and status of the to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1').then((response) => response.json()).then((json) => console.log(`the item "${json.title}" on the list has a status of ${json.completed}.`));


// STEP 7 - Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST', 
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 1,
		title: 'Created To Do List Item',
		userID: 1
	})
}).then((response) => response.json()).then((json) => console.log(json))

/*
STEP 8 - Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API
							- AND -
STEP 9 - Update a to do list item by changing the data structure to contain the following properties:
*/
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'Application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: 'Updated To Do List Item',
		userID: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));

/*
STEP 10 - Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API
							- AND -
STEP 11 - Update a to do list item by changing the status to complete and add a date when the status was changed
*/
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'Application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: '07/09/21',
		status: "Complete",
		title: 'delectus aut autem',
		userID: 1
	})
}).then((response) => response.json()).then((json) => console.log(json));


// STEP 12 - Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});



